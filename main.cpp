/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * Version 2, December 2004

 * Copyright (C) 2019 NOBODY

 * Everyone is permitted to copy and distribute verbatim or modified copies of this license document, and changing it is allowed as long as the name is changed.

 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
*/

#include <QCoreApplication>
#include <QtConcurrent>
#include <QMutexLocker>
#include <QThread>
#include <QMutex>

#include <functional>
#include <math.h>
#include <chrono>
#include <list>

constexpr int N = 100000;

class VelosipedThread: public QThread
{
  QMutex work_lock;
  std::list<void (*) ()> work;

  bool stopped;

  void workImpl()
  {
    QMutexLocker l(&work_lock);
    for(auto &i: work)
      i();
    work.clear();
  }

public:
  VelosipedThread(const std::initializer_list<void (*) ()> &init):
    work(init)
  { }

  void addTask(void (*func) ())
  {
    QMutexLocker l(&work_lock);
    work.emplace_back(func);
  }

  void stop()
  { stopped = true; }


  // QThread interface
protected:
  void run() override
  {
    stopped = false;

    while(!stopped)
      {
        workImpl();
        QThread::msleep(1);
      }
  }
};

namespace {
  float tempVal = 0.0;
}

void sin_stub()
{
  tempVal = std::sin(M_PI_2l);
}

class EventWorkerQt: public QObject
{
  Q_OBJECT

  QThread *th;

  void sin_stub_impl()
  {
    sin_stub();
  }

  void stop_impl()
  {
    moveToThread(QCoreApplication::instance()->thread());
    th->exit();
  }

public:
  EventWorkerQt():
    QObject(nullptr),
    th(new QThread(this))
  {
    connect(this, &EventWorkerQt::addSinStubTask, this, &EventWorkerQt::sin_stub_impl, Qt::QueuedConnection);
    connect(this, &EventWorkerQt::addStopTask, this, &EventWorkerQt::stop_impl, Qt::QueuedConnection);

    moveToThread(th);
  }

  void start()
  {
    th->start(QThread::HighestPriority);
  }

  void waitForFinished()
  {
    th->wait();
  }

signals:
  void addSinStubTask();
  void addStopTask();
};

namespace {
  VelosipedThread th {};

  EventWorkerQt event_worker;
}

void stopVelosiped()
{
  th.stop();
}

void benchVelosiped()
{
  auto t0 = std::chrono::steady_clock::now();
  th.start(QThread::HighestPriority);
  for(int i = 0 ; i < N; ++i)
    th.addTask(&sin_stub);
  th.addTask(&stopVelosiped);

  th.wait();
  const int elapsed = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - t0).count();
  std::printf("Velosiped (TM): %.5f ms\n", 0.001 * elapsed);
}

void benchEventWorker()
{
  auto t0 = std::chrono::steady_clock::now();

  event_worker.start();
  for(int i = 0 ; i < N; ++i)
    event_worker.addSinStubTask();
  event_worker.addStopTask();
  event_worker.waitForFinished();

  const int elapsed = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - t0).count();
  std::printf("Qt QObject worker: %.5f ms\n", 0.001 * elapsed);
}

void benchThreadPool()
{
  QThreadPool worker;
  worker.setMaxThreadCount(1);

  auto t0 = std::chrono::steady_clock::now();
  std::list<void (*) ()> work;
  for(int i = 0 ; i < N; ++i)
    work.push_back(&sin_stub);

  QtConcurrent::run(&worker, [&work] {
      for(auto &i: work)
        i();
      work.clear();
    }).waitForFinished();

  const int elapsed = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - t0).count();
  std::printf("Qt Concurrent: %.5f ms\n", 0.001 * elapsed);
}

int main(int argc, char *argv[])
{
  QCoreApplication app(argc, argv);

  QThreadPool benchmarkPool;
  benchmarkPool.setMaxThreadCount(1);

  QtConcurrent::run(&benchmarkPool, [&app] {
      benchEventWorker();
      benchVelosiped();
      benchThreadPool();
      app.quit();
    });

  return app.exec();
}

#include "main.moc"
